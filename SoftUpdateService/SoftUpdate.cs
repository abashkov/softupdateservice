﻿using System;
using System.IO;
using Airtime.Kiosk.Library;
using Airtime.Logs;

namespace Airtime.Kiosk.Services
{
    public static class SoftUpdate
    {

        static Scheduler scheduler = null;
        static System.Windows.Forms.Timer timer = null;
        static int tryCount;


        public static void Start()
        {
            if (scheduler == null) // first start
            {
                timer = new System.Windows.Forms.Timer();
                timer.Tick += new EventHandler(timer_Tick);
                timer.Interval = 60000;
                scheduler = new Scheduler(Settings.Instance.SoftUpdate.UpdateTime);
                scheduler.OnSchedulerEvent += new SchedulerNotifyEvent(StartCycle);
            }
            if (!scheduler.Started())
                scheduler.Start();
        }


        public static void Stop()
        {
            if (scheduler == null)
                return;
            if (scheduler.Started())
            {
                StopCycle();
                scheduler.Stop();
            }
        }


        static void StartCycle()
        {
            Settings.SoftUpdateSettings settings = Settings.Instance.SoftUpdate;
            int Interval = settings.TryInterval;
            int MaxTryCount = settings.TryCount;
            if (MaxTryCount <= 0)
                return;
            Updater.RunUpdating();
            if ((Interval > 0) && (MaxTryCount > 1))
            {
                tryCount = 1;
                timer.Enabled = true;
            }
        }


        static void StopCycle()
        {
            if (timer.Enabled)
                timer.Enabled = false;
            Updater.FinishUpdating();
        }


        static void timer_Tick(object sender, EventArgs e)
        {
            if (Updater.IsRunning())
                return;

            if (!Updater.NeedContinueUpdating())
            {
                timer.Enabled = false;
                return;
            }

            Settings.SoftUpdateSettings settings = Settings.Instance.SoftUpdate;
            int MaxTryCount = settings.TryCount;
            if (tryCount >= MaxTryCount)
            {
                timer.Enabled = false;
                Logger.LogString("Попытки обновить ПО исчерпаны");
            }
            else
            {
                if (DateTime.Now.Subtract(Updater.LastUpdateTryDateTime()).Minutes >= settings.TryInterval)
                {
                    tryCount++;
                    Logger.LogString(string.Format("Старт попытки {0} (из {1}) обновить ПО...", tryCount, MaxTryCount));
                    Updater.RunUpdating();
                }
            }
        }


        public static void ProcessSoftwareUpdaterLogs()
        {
            string UpdaterLog = Updater.UpdatesFolder + "\\Log.log";
            try
            {
                if (!File.Exists(UpdaterLog))
                    return;
                Logger.OpenHeader("Записи из журнала обновления ПО");
                try
                {
                    using (StreamReader sr = new StreamReader(UpdaterLog))
                    {
                        string line = sr.ReadLine();
                        while (line != null)
                        {
                            if (line.Length > 22)
                            {
                                if (line.IndexOf("ERROR: ") == 22)
                                    Logger.LogError("ПРОГРАММА ОБНОВЛЕНИЯ ПО - " + line.Remove(22, 7));
                                else
                                    Logger.LogString("ПРОГРАММА ОБНОВЛЕНИЯ ПО - " + line);
                            }
                            line = sr.ReadLine();
                        }
                    }
                }
                finally
                {
                    LogFile.CloseHeader();
                }
                File.Delete(UpdaterLog);
            }
            catch (Exception e)
            {
                Logger.LogError("Ошибка при обработке логов программы обновления ПО: " + e.Message);
            }
        }


    }
}
