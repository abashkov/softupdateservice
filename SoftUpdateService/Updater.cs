﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net;
using System.IO;
using Airtime.Kiosk.Library;
using System.Diagnostics;
using System.Threading;
using Microsoft.Win32;
using Airtime.Logs;

namespace Airtime.Kiosk.Services
{

    static class Updater
    {

        static DateTime lastUpdateTryDateTime = DateTime.MinValue;
        static bool needContinueUpdating = false;
        static bool terminated = false;
        static bool running = false;
        static Thread thread = null;

        internal static string UpdatesFolder = Folders.AirtimeDirectory + "\\Updates";        


        #region members of class Updater
        public static void RunUpdating()
        {
            if (IsRunning())
                return;
            terminated = false;
            thread = new Thread(new ThreadStart(Run));
            thread.Start();
        }


        public static void FinishUpdating()
        {
            if (!IsRunning())
                return;
            terminated = true;
            Thread.Sleep(3000);
            if (thread != null)
            {
                if (!thread.Join(2000))
                    thread.Abort();
            }
        }


        public static bool IsRunning()
        {
            return running;
        }


        public static bool NeedContinueUpdating()
        {
            return needContinueUpdating;
        }


        public static DateTime LastUpdateTryDateTime()
        {
            return lastUpdateTryDateTime;
        }


        static void Run()
        {
            running = true;
            if (!Directory.Exists(UpdatesFolder))
                Directory.CreateDirectory(UpdatesFolder);
            string FtpFolder;
            using (DBConnection connection = new DBConnection())
            {
                FtpFolder = connection.GetSoftFtpFolder();
            }
            if (FtpFolder != "")
            {
                Logger.LogString(string.Format("Начало обновления ПО с ресурса \"{0}\"", FtpFolder));
                bool CommonUpdateResult = DoUpdate(FtpFolder + "/Common");
                if (CommonUpdateResult)
                {
                    Logger.LogString("Работа с общим для всех киосков обновлением успешно завершена");
                    if (ExistsPersonalFtpFolder(FtpFolder))
                    {
                        if (terminated)
                        {
                            Logger.LogString("Обновление ПО было прервано");
                            needContinueUpdating = true;
                        }
                        else
                        {
                            bool PersonalUpdateResult = DoUpdate(string.Format("{0}/Personal/{1}", FtpFolder, Settings.Instance.KioskName));
                            if (PersonalUpdateResult)
                                Logger.LogString("Работа с персональным обновлением для этого киоска успешно завершена");
                            else
                                Logger.LogString("Скачивание персонального обновления для этого киоска завершилось неудачно");
                            needContinueUpdating = !PersonalUpdateResult;
                        }
                    }
                    else
                    {
                        needContinueUpdating = false;
                    }
                }
                else
                {
                    Logger.LogString("Скачивание общего для всех киосков обновления завершилось неудачно");
                    needContinueUpdating = true;
                }
            }
            lastUpdateTryDateTime = DateTime.Now;
            running = false;
        }


        static bool DoUpdate(string FtpFolder)
        {
            try
            {
                List<string> rars = GetLatestArchives(FtpFolder);
                if (terminated)
                {
                    Logger.LogString("Обновление ПО было прервано");
                    return false;
                }
                if (rars.Count == 0)
                {
                    Logger.LogString(string.Format("В папке \"{0}\" нет новых обновлений", FtpFolder));
                    return true;
                }
                List<Update> updates = GetUpdatesFromArchives(rars);
                Logger.LogString(string.Format("В папке \"{0}\" найдено {1} обновление(й) в {2} архиве(ах)", FtpFolder, updates.Count, rars.Count));
                foreach (Update update in updates)
                {
                    if (terminated)
                    {
                        Logger.LogString("Обновление ПО было прервано");
                        return false;
                    }
                    if (!update.DownloadIfNeed(FtpFolder))
                        return false;
                    if (UnpackAndVerify(update))
                    {
                        string updaterExe = UpdatesFolder + "\\" + update.Name + "\\bin\\SoftwareUpdater.exe";
                        if (File.Exists(updaterExe))
                        {
                            Logger.LogString(string.Format("В обновлении \"{0}\" обнаружен файл SoftwareUpdater.exe; существующий файл SoftwareUpdater.exe будет заменён на новый", update.Name));
                            string destExe = Folders.BinDirectory + "\\SoftwareUpdater.exe";
                            File.Copy(updaterExe, destExe, true);
                        }
                        Registry.SetValue(@"HKEY_LOCAL_MACHINE\SOFTWARE\Airtime\AirtimeKiosk", "NeedRunSoftwareUpdater", 1, RegistryValueKind.DWord);
                    }
                    else
                    {
                        string dir = Path.Combine(UpdatesFolder, update.Name);
                        if (Directory.Exists(dir))
                        {
                            try
                            {
                                Directory.Delete(dir, true);
                            }
                            catch (Exception e)
                            {
                                Logger.LogError(string.Format("Невозможно удалить папку {0}: {1}", dir, e.Message));
                            }
                        }
                        return false;
                    }
                }
                return true;                
            }
            catch (Exception e)
            {
                Logger.LogError("Исключительная ситуация при обновлении ПО: " + e.Message);
                return false;
            }
        }


        static bool UnpackAndVerify(Update update)
        {
            if (!update.UnpackArchive())
                return false;

            // verify checksums:
            string updateBinDir = UpdatesFolder + "\\" + update.Name + "\\bin";
            if (!Directory.Exists(updateBinDir))
                return true;
            string[] md5Files = Directory.GetFiles(updateBinDir, "*.md5");
            string md5;
            string sourceFile;
            foreach (string md5File in md5Files)
            {
                if (!Security.ParseMD5File(md5File, out md5, out sourceFile))
                    return false;
                else
                {
                    if (File.Exists(sourceFile))
                    {
                        if (!Security.CheckMD5ForFile(sourceFile, md5))
                            return false;
                    }
                }
            }
            return true;
        }


        static bool ExistsPersonalFtpFolder(string FtpFolder)
        {
            try
            {
                string FtpUser = "";
                string FtpPassword = "";
                using (DBConnection connection = new DBConnection())
                {
                    connection.GetSoftFtpParameters(ref FtpUser, ref FtpPassword);
                }
                FtpWebRequest request = (FtpWebRequest)WebRequest.Create(FtpFolder + "/Personal");
                request.Method = WebRequestMethods.Ftp.ListDirectory;
                request.Credentials = new NetworkCredential(FtpUser, FtpPassword);
                FtpWebResponse response = (FtpWebResponse)request.GetResponse();
                try
                {
                    using (StreamReader sr = new StreamReader(response.GetResponseStream(), Encoding.UTF8))
                    {
                        bool res = false;
                        string KioskName = Settings.Instance.KioskName;
                        string line;
                        while ((line = sr.ReadLine()) != null)
                        {
                            if (line == KioskName)
                                res = true;
                        }
                        return res;
                    }
                }
                finally
                {
                    if (response != null)
                        response.Close();
                }
            }
            catch (Exception e)
            {
                Logger.LogError("Исключительная ситуация при запросе существования папки с персональными обновлениями для этого киоска: " + e.Message);
                return true;
            }
        }


        static List<string> GetLatestArchives(string FtpFolder)
        {
            List<string> ftpElements = new List<string>();
            try
            {
                string FtpUser = "";
                string FtpPassword = "";
                using (DBConnection connection = new DBConnection())
                {
                    connection.GetSoftFtpParameters(ref FtpUser, ref FtpPassword);
                }
                FtpWebRequest request = (FtpWebRequest)WebRequest.Create(FtpFolder);
                request.Method = WebRequestMethods.Ftp.ListDirectory;
                request.Credentials = new NetworkCredential(FtpUser, FtpPassword);
                FtpWebResponse response = (FtpWebResponse)request.GetResponse();
                try
                {
                    using (StreamReader sr = new StreamReader(response.GetResponseStream(), Encoding.UTF8))
                    {
                        string line;
                        while ((line = sr.ReadLine()) != null)
                            ftpElements.Add(line);
                    }
                }
                finally
                {
                    if (response != null)
                        response.Close();
                }
                string CurrentVersion = AirtimeSoftwareVersion.GetSoftwareVersion();
                List<string> rars = new List<string>();
                foreach (string ftpElement in ftpElements)
                {
                    if (ftpElement.StartsWith("Update") && ftpElement.EndsWith(".rar") && (ftpElement.Length > 10))
                    {
                        string updateNumber = Path.GetFileNameWithoutExtension(Path.GetFileNameWithoutExtension(ftpElement.Substring(6))); // remove extension and "partXX"
                        if (string.Compare(updateNumber, CurrentVersion) > 0) // updateNumber is greater than current version
                            rars.Add(ftpElement);
                    }
                }
                return rars;
            }
            catch (Exception e)
            {
                Logger.LogError("Исключительная ситуация при запросе позднейших архивов с обновлениями: " + e.Message);
                throw;
            }
        }


        static List<Update> GetUpdatesFromArchives(List<string> archives)
        {
            List<Update> updates = new List<Update>();
            archives.Sort();
            foreach (string archive in archives)
            {
                string updateName = Path.GetFileNameWithoutExtension(Path.GetFileNameWithoutExtension(archive)); // remove extension and "partXX"
                int index = updates.FindIndex(delegate(Update update){return (update.Name == updateName);});
                if (index == -1)
                    updates.Add(new Update(updateName, archive));
                else
                    updates[index].AddArchive(archive);
            }
            return updates;
        }
        #endregion


        #region class Update - private class of class Updater
        class Update
        {

            string name;
            List<string> archives = new List<string>();


            public string Name
            {
                get
                {
                    return name;
                }
            }


            public Update(string aName, string firstArchive)
            {
                name = aName;
                archives.Add(firstArchive);
            }


            public bool DownloadIfNeed(string FtpFolder)
            {
                if (Directory.Exists(Path.Combine(UpdatesFolder, name)))
                    return true;

                try
                {
                    foreach (string archive in archives)
                    {
                        if (terminated)
                        {
                            Logger.LogString("Загрузка обновления была прервана");
                            return false;
                        }
                        if (!File.Exists(Path.Combine(UpdatesFolder, archive)))
                        {
                            if (!DownloadFile(FtpFolder, archive))
                                return false;
                        }
                    }

                    if (TestArchive())
                        return true;
                    else
                    {
                        foreach (string archive in archives)
                            File.Delete(Path.Combine(UpdatesFolder, archive));
                        return false;
                    }
                }
                catch (Exception e)
                {
                    Logger.LogError(string.Format("Невозможно загрузить обновление \"{0}\": {1}", name, e.Message));
                    return false;
                }
            }


            public void AddArchive(string rar)
            {
                archives.Add(rar);
                archives.Sort();
            }


            bool TestArchive()
            {
                const int ProcessTimeoutInSec = 10;
                archives.Sort();
                string ArchiveFullName = Path.Combine(UpdatesFolder, archives[0]);
                Logger.LogString("Старт проверки архива " + ArchiveFullName);
                ProcessStartInfo psi = new ProcessStartInfo("unrar.exe");
                psi.Arguments = string.Format("t {0}", ArchiveFullName);
                psi.CreateNoWindow = true;
                psi.UseShellExecute = false;
                Process test = null;
                try
                {
                    test = System.Diagnostics.Process.Start(psi);
                }
                catch (Exception e)
                {
                    Logger.LogError("Исключительная ситуация при проверке архива: " + e.Message);
                    return false;
                }
                if (test != null)
                {
                    int sec = 0;
                    while (!terminated && (sec < ProcessTimeoutInSec))
                    {
                        if (test.HasExited)
                        {
                            int ExitCode = test.ExitCode;
                            if (ExitCode != 0)
                            {
                                Logger.LogError(string.Format("Процесс проверки архива завершился с кодом {0}", ExitCode));
                                return false;
                            }
                            else
                            {
                                Logger.LogString("Проверка архива завершилась успешно");
                                return true;
                            }
                        }
                        else
                        {
                            Thread.Sleep(1000);
                            sec++;
                        }
                    }
                    test.Kill();
                    Logger.LogError("Превышено время проверки архива, процесс проверки завершён принудительно");
                }
                return false;
            }


            public bool UnpackArchive()
            {
                const int ProcessTimeoutInSec = 10;
                archives.Sort();
                string ArchiveFullName = Path.Combine(UpdatesFolder, archives[0]);
                Logger.LogString("Старт распаковки архива " + ArchiveFullName);
                ProcessStartInfo psi = new ProcessStartInfo("unrar.exe");
                psi.Arguments = string.Format("x {0} -o+ {1}\\", ArchiveFullName, UpdatesFolder);
                psi.CreateNoWindow = true;
                psi.UseShellExecute = false;
                Process unrar = null;
                try
                {
                    unrar = System.Diagnostics.Process.Start(psi);
                }
                catch (Exception e)
                {
                    Logger.LogError("Исключительная ситуация при распаковке архива: " + e.Message);
                    return false;
                }
                if (unrar != null)
                {
                    int sec = 0;
                    while (!terminated && (sec < ProcessTimeoutInSec))
                    {
                        if (unrar.HasExited)
                        {
                            int ExitCode = unrar.ExitCode;
                            if (ExitCode != 0)
                            {
                                Logger.LogError(string.Format("Процесс распаковки архива завершился с кодом {0}", ExitCode));
                                return false;
                            }
                            else
                            {
                                Logger.LogString("Распаковка архива завершилась успешно");
                                return true;
                            }
                        }
                        else
                        {
                            Thread.Sleep(1000);
                            sec++;
                        }
                    }
                    unrar.Kill();
                    Logger.LogError("Превышено время распаковки архива, процесс распаковки завершён принудительно");
                }
                return false;
            }


            bool DownloadFile(string FtpFolder, string ArchiveName)
            {
                Logger.LogString(string.Format("Старт скачивания архива {0} из папки \"{1}\"", ArchiveName, FtpFolder));
                try
                {
                    string FtpUser = "";
                    string FtpPassword = "";
                    using (DBConnection connection = new DBConnection())
                    {
                        connection.GetSoftFtpParameters(ref FtpUser, ref FtpPassword);
                    }
                    string ftpUri = Path.Combine(FtpFolder, ArchiveName);
                    FtpWebRequest request = (FtpWebRequest)WebRequest.Create(ftpUri);
                    request.Method = WebRequestMethods.Ftp.DownloadFile;
                    request.Credentials = new NetworkCredential(FtpUser, FtpPassword);
                    request.UseBinary = true;
                    FtpWebResponse response = (FtpWebResponse)request.GetResponse();
                    try
                    {
                        using (Stream inputStream = response.GetResponseStream())
                        {
                            using (FileStream outputStream = new FileStream(Path.Combine(UpdatesFolder, ArchiveName), FileMode.Create))
                            {
                                int bufferSize = 2048;
                                byte[] buffer = new byte[bufferSize];
                                int readCount;
                                do
                                {
                                    readCount = inputStream.Read(buffer, 0, bufferSize);
                                    outputStream.Write(buffer, 0, readCount);
                                } while (readCount > 0);
                            }
                        }
                    }
                    finally
                    {
                        response.Close();
                    }
                }
                catch (Exception e)
                {
                    Logger.LogError("Исключительная ситуация при скачивании архива: " + e.Message);
                    return false;
                }
                Logger.LogString(string.Format("Архив {0} успешно скачан", ArchiveName));
                return true;
            }

        }
        #endregion

    }

}
